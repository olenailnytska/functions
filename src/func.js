const getSum = (str1, str2) => {
  if (typeof str1 !== 'string' || typeof str2 !== 'string') {
    return false;
  }
  if (!/^\d*$/.test(str1) || !/^\d*$/.test(str2)) {
    return false;
  }
  str1 = str1 === '' ? BigInt(0) : BigInt(str1);
  str2 = str2 === '' ? BigInt(0) : BigInt(str2);
  return str1 + str2 + '';
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let posts = 0;
  let comments = 0;
  for (let post of listOfPosts) {
    if (post.author === authorName) {
      posts++;
    }
    if (post.comments !== undefined) {
      for (let comment of post.comments) {
        if (comment.author === authorName) {
          comments++;
        }
      }
    }
  }
  return 'Post:' + posts + ',comments:' + comments;
};

const tickets = (people) => {
  let change = 0;
  for (let money of people) {
    if (+money === 25) {
      change += 25;
    }
    if (+money === 50) {
      if (change >= 25) {
        change += 25;
      } else {
        return 'NO';
      }
    }
    if (+money === 100) {
      if (change >= 75) {
        change += 25;
      } else {
        return 'NO';
      }
    }
  }
  return 'YES';
};


module.exports = { getSum, getQuantityPostsByAuthor, tickets };
